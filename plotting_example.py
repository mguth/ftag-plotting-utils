"""Produce roc curves from tagger output and labels."""
import numpy as np

from umami.helper_tools import get_class_label_ids
from puma.utils import logger
from pathlib import Path
import pandas as pd
import h5py
from plot_utils import Tagger, Results

n_jets = 1_000_000
file_dir = "/srv/beegfs/scratch/groups/dpnc/atlas/FTag/samples/preprocessed-umami/r22-2022-07-p5169/hybrids/"
net_dir = "/srv/beegfs/scratch/groups/dpnc/atlas/FTag/samples/gnn-samples/results_share/transformer-matt/FlavTrans_v1"
net_dir_all_aux = "/srv/beegfs/scratch/groups/dpnc/atlas/FTag/samples/gnn-samples/results_share/transformer-matt/AllAux/"
file_name = "inclusive_testing_ttbar_PFlow.h5"


dl1d = Tagger("DL1dv01")
dl1d.label = "DL1dv01 ($f_{c}=0.018$)"
dl1d.f_c = 0.018
dl1d.reference = True
gn1 = Tagger("GN120220509")
gn1.label = "GN1 ($f_{c}=0.05$)"
gn1.f_c = 0.05
trans = Tagger("Trans")
trans.label = "Transformer ($f_{c}=0.018$)"
trans.f_c = 0.018
trans_allaux = Tagger("Trans_allaux")
trans_allaux.label = "Transformer all aux ($f_{c}=0.018$)"
trans_allaux.f_c = 0.018

results = Results()
results.add(dl1d)
results.add(gn1)
results.add(trans)
results.add(trans_allaux)

results.sig_eff = np.linspace(0.6, 0.95, 20)
results.atlas_second_tag = (
    "$\\sqrt{s}=13$ TeV, PFlow jets \n$t\\bar{t}$, $20<p_{T}<250$ GeV"
)

## Load the truth labels and dips scores into a pandas dataset
with h5py.File(Path(file_dir) / file_name, "r") as file:
    df = pd.DataFrame(
        file["jets"].fields(
            [
                "HadronConeExclTruthLabelID",
                "pt_btagJes",
                *[name + fl for name in results.model_names[:-2] for fl in dl1d.flvs],
            ]
        )[:n_jets]
    )
df["pt_btagJes"] = df["pt_btagJes"] / 1e3
# Get the class ids for removing
class_ids = get_class_label_ids(dl1d.class_labels)
# Remove all jets which are not trained on
df.query(f"HadronConeExclTruthLabelID in {class_ids}", inplace=True)

## Load the network's exported values

with h5py.File(Path(f"{net_dir}/{file_name}"), "r") as file:
    net_probs = file["probs"][: len(df)]
    df["Trans_pu"] = net_probs[:, 0]
    df["Trans_pc"] = net_probs[:, 1]
    df["Trans_pb"] = net_probs[:, 2]
with h5py.File(Path(f"{net_dir_all_aux}/{file_name}"), "r") as file:
    net_probs = file["probs"][: len(df)]
    df["Trans_allaux_pu"] = net_probs[:, 0]
    df["Trans_allaux_pc"] = net_probs[:, 1]
    df["Trans_allaux_pb"] = net_probs[:, 2]
df.query(f"pt_btagJes < 250", inplace=True)
logger.info("caclulate tagger discriminants")

gn1.reference = True
# results.plot_discs(df, "trans-disc.png", ["DL1dv01"])
results.plot_rocs(df, "roc-trans.png")

results.atlas_second_tag = (
    "$\\sqrt{s}=13$ TeV, PFlow jets \n$t\\bar{t}$, $20<p_{T}<250$ GeV\n" "70% WP"
)
dl1d.disc_cut = 3.493
gn1.disc_cut = 3.642
trans.disc_cut = 2.794
# trans.working_point = 0.7

results.plot_pt_perf(
    df,
    "transformer",
    bins=[20, 30, 40, 60, 85, 110, 140, 175, 250],
    fixed_eff_bin=False,
)

results.atlas_second_tag = (
    "$\\sqrt{s}=13$ TeV, PFlow jets \n$t\\bar{t}$, $20<p_{T}<250$ GeV\n"
    "70% WP per bin"
)

results.plot_pt_perf(
    df,
    "transformer_fixed_per_bin",
    bins=[20, 30, 40, 60, 85, 110, 140, 175, 250],
    fixed_eff_bin=True,
    working_point=0.7,
    disc_cut=None,
)
